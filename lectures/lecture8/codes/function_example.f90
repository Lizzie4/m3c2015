!Simple example of Fortran function
!Two numbers are added together

program function_example
    implicit none
    double precision :: x,y,z
    double precision, external :: sumxy,sumxy_alternate !function called in main program

    x = 2.0
    y = 3.0
    z = sumxy(x,y)

    print *, 'x,y,z=',x,y,z

end program function_example


!--------------------------------------------------------------------
!function to sum two numbers provided as input
!note that the function name is a variable whose
!type can be declared in the header
function sumxy(x,y)
    implicit none
    double precision, intent(in) :: x,y
    double precision :: sumxy

    sumxy = x + y
end function sumxy

!--------------------------------------------------------------------
!Note that the variable corresponding to the
!function name can also be declared in the header
!
double precision function sumxy_alternate(x,y)
    implicit none
    double precision, intent(in) :: x,y

    sumxy_alternate = x + y
end function sumxy_alternate

!--------------------------------------------------------------------
